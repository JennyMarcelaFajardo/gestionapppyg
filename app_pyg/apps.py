from django.apps import AppConfig


class AppPygConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_pyg'
